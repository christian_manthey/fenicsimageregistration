"""
Test fenics interpolation between meshes.
"""

import fenics as fn
import numpy as np

n=100
n2 = 40

mesh0 = fn.UnitSquareMesh(n,n)  # Initial image mesh
mesh1 = fn.UnitSquareMesh(n2,n2)  # mesh for solving PDEs
mesh2 = fn.UnitSquareMesh(n2,n2)  # mesh for moving vertices


""" Define function spaces """
V0 = fn.FunctionSpace(mesh0, 'P', 2)
V1 = fn.FunctionSpace(mesh1, 'P', 3)
V2 = fn.FunctionSpace(mesh2, 'P', 3)


# Make expression
v = fn.Expression("sin(10.0*x[0])*sin(10.0*x[1])", degree=5)


# Create function on V0 and interpolate v
v0 = fn.Function(V0)
v0.interpolate(v)


# get mesh1's coordinates
mesh1_x = mesh1.coordinates()


# change mesh1's positions
mesh1_x[:,0] = np.sin(mesh1_x[:,0])**2 + 0.2
v = mesh1_x >= 1.0
mesh1_x[v] = mesh1_x[v] - 1.0


# Interpolate v3 onto mesh2
v1 = fn.Function(V1)
v1.interpolate(v0)

v2 = fn.Function(V2)
v2.vector()[:] = v1.vector().array()


fn.plot(v0)
fn.plot(v1)
fn.plot(v2)
fn.plot(mesh1, interactive=True)
