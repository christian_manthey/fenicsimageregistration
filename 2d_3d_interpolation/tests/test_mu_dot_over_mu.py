"""
Checks the problem class.
"""

import numpy as np
from source.problem import Problem
import fenics as fn

# Parameters
n = 40
step_num = 2

# Make two 3D numpy arrays
sz = 100
gaussian_std = 2.0
bg_den = 0.2  # background density
x11, x12 = (10, 35)  # first image start and end points (horizontally)
x21, x22 = (20, 60)
y11, y12 = (30, 75)  # second image start and end points (horizontall)
y21, y22 = (40, 50)
img0 = bg_den * np.ones((sz, sz), dtype='float')
img0[y11:y12, x11:x12] = 1.0
img1 = bg_den * np.ones((sz, sz), dtype='float')
img1[y21:y22, x21:x22] = 1.0


# Make list of images
imgs = [img0, img1]



# Initialize problem class
prob = Problem(imgs, n, step_num)

# Show the first mu dot over mu
fn.plot(prob.mu_dot_over_mu[0], title='mu dot over mu [0]', interactive=True)
