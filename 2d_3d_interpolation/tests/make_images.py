import fenics as fn
import numpy as np
from matplotlib import pylab as plt
from source import utils, utils_fn
from source.problem import Problem
from source.diffeo import Diffeo
from matplotlib import pylab as plt

# fn.parameters["num_threads"] = 4

# parameters
step_num = 5  # number of time steps
n = 100  # number of mesh elements

# Make image list
sz = 100  # size of images
imgs = utils.make_image_list(sz)

for a in range(2):
    plt.figure(a), plt.imshow(imgs[a], cmap='gray')

plt.pause(10)
