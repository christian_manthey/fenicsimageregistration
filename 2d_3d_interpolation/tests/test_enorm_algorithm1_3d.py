import fenics as fn
import numpy as np
from matplotlib import pylab as plt
from source import utils, utils_fn
from source.problem import Problem
from source.diffeo import Diffeo
from matplotlib import pylab as plt
import time

t0 = time.time()

# solver.parameters["linear_solver"] = "mumps"
# fn.parameters["num_threads"] = 6

# parameters
save = 0  # Save the results (1) or not (0)
step_num = 5  # number of time steps
n = 20  # number of mesh elements

# Make image list
sz = 20  # size of images
imgs = utils.make_image_list_3d(sz)
# plt.figure(), plt.imshow(imgs[0]), plt.pause(100)


# Make problem class
prob = Problem(imgs, n, step_num, fam='P', deg=1)

# Make diffeomorphism class
diff = Diffeo(prob)

# project img0 into V1
img0_v1 = fn.project(prob.imgs0_fn[0], prob.V1, solver_type='cg')

# For product of img0 and mu_dot_over_mu
h_fn = fn.Function(prob.V1)


A = utils_fn.assemble_a_poisson(img0_v1, prob.V1)


for a in range(step_num):
    print('Iteration number %d out of %d. ' %(a, step_num))
    mu_dot_over_mu = diff.compose(prob.mu_dot_over_mu[a], diff.dinv_phi)
    # h = mu_dot_over_mu * prob.imgs0_fn[0]
    h = mu_dot_over_mu * prob.imgs0_fn[0]**(prob.dim/2.0)
    h_fn.vector()[:] = fn.project(h, prob.V1, solver_type='cg').vector().array()
    f = utils_fn.solve_diffeo_problem(h_fn, prob.V1, img0_v1, A=A)
    v = utils_fn.grad(f, img0_v1, prob.dim)
    diff.update_phi(v)

    # print('The norm of image 0 is: ', fn.assemble(img0_v1*fn.dx))
    # fn.plot(img0_v1, title='Image 0')
    # fn.plot(mu_dot_over_mu, title='mu dot over mu')
    # fn.plot(v, title='velocity')
    # fn.plot(diff.dinv_psi)
    # fn.interactive()

print('The time to run this algorithm was: ', int(time.time() - t0), ' seconds.')


# Calculate transformations
diff.calc_det_jac()
mu0_transformed = fn.project(diff.pull_back(prob.imgs0_fn[0]), prob.V0, solver_type='cg')

if save == 1:
    # Save file for paraview viewing
    dphi = fn.Function(prob.T0)
    dphi.vector()[:] = fn.project(diff.dphi, prob.T0, solver_type='cg').vector().array()
    vtkfile = fn.File('data/%idphi.pvd' %n)
    vtkfile << dphi
    vtkfile = fn.File('data/%idet_jac.pvd' %n)
    vtkfile << fn.project(diff.det_jac_phi, prob.V0, solver_type='cg')
    mu0tfile = fn.File('data/%imu0_transformed.pvd' %n)
    mu0tfile << mu0_transformed
    mu1file = fn.File('data/%imu1.pvd' %n)
    mu1file << fn.project(prob.imgs0_fn[1], prob.V0, solver_type='cg')
    mu0file = fn.File('data/%imu0.pvd' %n)
    mu0file << fn.project(prob.imgs0_fn[0], prob.V0, solver_type='cg')


# Move mesh
diff.move_mesh2(diff.dinv_phi)

if save == 1:
    # Save file for paraview viewing
    meshfile = fn.File('data/%iwarp.pvd' %n)
    meshfile << prob.mesh2

diff.move_mesh2(2.0*diff.dinv_phi)

if save == 1:
    # Save file for paraview viewing
    meshfile = fn.File('data/%i_exaggerated_warp.pvd' %n)
    meshfile << prob.mesh2



if n < 40:
    # Plot the mesh, transformed density
    print('The error between mu0 and mu1 is: ', fn.norm(fn.project(prob.imgs0_fn[0] - prob.imgs0_fn[1], prob.V1)))
    print('The error is: ', fn.norm(fn.project(mu0_transformed - prob.imgs0_fn[1],prob.V1, solver_type='cg')))
    print('The error between mu0_trans & mu0 is: ', fn.norm(fn.project(mu0_transformed - prob.imgs0_fn[0],prob.V1, solver_type='cg')))

    fn.plot(mu0_transformed - prob.imgs0_fn[1], title='Difference in densities')
    fn.plot(prob.mu_dot_over_mu[0], title='mu dot over mu')
    fn.plot(prob.imgs0_fn[1], title = 'mu 1')
    fn.plot(mu0_transformed, title = 'mu 0 tranformed')
    fn.plot(prob.mesh2, title='warp')
    fn.interactive()




# Print the limits
# print('limits are: ', prob.lim)





#end
