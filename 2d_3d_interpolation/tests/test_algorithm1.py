import fenics as fn
import numpy as np
from matplotlib import pylab as plt
from source import utils, utils_fn
from source.problem import Problem
from source.diffeo import Diffeo
from matplotlib import pylab as plt

# fn.parameters["num_threads"] = 4

# parameters
step_num = 5  # number of time steps
n = 100  # number of mesh elements

# Make image list
sz = 100  # size of images
imgs = utils.make_image_list(sz)
# plt.figure(), plt.imshow(imgs[0]), plt.pause(100)


# Make problem class
prob = Problem(imgs, n, step_num, fam='P', deg=1)

# print('The norm of image 0 is: ', fn.assemble(prob.imgs0_fn[0]*fn.dx))

# Make diffeomorphism class
diff = Diffeo(prob)

# project img0 into V1
img0_v1 = fn.project(prob.imgs0_fn[0], prob.V1, solver_type='cg')

# For product of img0 and mu_dot_over_mu
h_fn = fn.Function(prob.V1)

A = utils_fn.assemble_a_poisson(img0_v1, prob.V1)

for a in range(step_num):
    print('Iteration number %d out of %d. ' %(a, step_num))
    mu_dot_over_mu = diff.compose(prob.mu_dot_over_mu[a], diff.dinv_phi)
    h = mu_dot_over_mu * prob.imgs0_fn[0]
    # h = mu_dot_over_mu * prob.imgs0_fn[0]**prob.dim/2.0
    h_fn.vector()[:] = fn.project(h, prob.V1, solver_type='cg').vector().array()
    f = utils_fn.solve_diffeo_problem(h_fn, prob.V1, img0_v1, A=A)
    v = utils_fn.grad(f, img0_v1, prob.dim)
    diff.update_phi(v)
    # print('The norm of image 0 is: ', fn.assemble(img0_v1*fn.dx))
    # fn.plot(img0_v1, title='Image 0')
    # fn.plot(mu_dot_over_mu, title='mu dot over mu')
    # fn.plot(v, title='velocity')
    # fn.plot(diff.dinv_psi)
    # fn.interactive()


# Calculate transformations
diff.calc_det_jac()
mu0_transformed = diff.pull_back(prob.imgs0_fn[0])


# Show the difference
fn.plot(mu0_transformed - fn.project(prob.imgs0_fn[1], prob.V1, solver_type='cg'), title='Difference')



# Move mesh
diff.move_mesh2(diff.dinv_phi)

# Plot the mesh, transformed density
fn.plot(mu0_transformed)
fn.plot(prob.mesh2)
fn.interactive()

# Print the limits
# print('limits are: ', prob.lim)


#end
