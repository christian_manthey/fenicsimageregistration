"""
Test mpi
"""
from petsc4py import PETSc
from mpi4py import MPI
import fenics as fn
import numpy as np


# make mpi class instance
comm = MPI.COMM_WORLD
rank = comm.Get_rank()

print('rank is: ', rank)


def init_fe():
    mesh = fn.UnitSquareMesh(10, 10)
    V = fn.FunctionSpace(mesh, 'P', 1)
    f = fn.Expression('sin(x[0])*sin(x[1])', degree=1)
    u = fn.TrialFunction(V)
    v = fn.TestFunction(V)

    a = fn.inner(fn.grad(u), fn.grad(v)) * fn.dx
    l = f * v * fn.dx

    f = fn.Function(V).vector().array()
    f = np.zeros((100,))
    # print(np.shape(f))
    # exit()
    A = fn.assemble(a)
    b = fn.assemble(l)
    data = np.arange(100, dtype='float64')
    return (data)

f = init_fe()
#
# if rank == 0:
#     bs = np.zeros((10,10), dtype='float64')
#
#
#
# a, l, mesh, V = init_fe()
#
# u = fn.Function(V)
# fn.solve(a==l,u)
# comm.Barrier()
#
# # plot solution
# fn.plot(u)
# fn.interactive()

if rank == 0:
    data = init_fe()
    # data = np.arange(100, dtype='float64')
    comm.bcast(data, root=0)
else:
    data = np.empty(100, dtype='float64')
    data = comm.bcast(data, root=0)

print(rank, np.max(data))
