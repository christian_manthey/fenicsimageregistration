"""
Test mpi
"""

import fenics as fn
import numpy as np
from source.diffeo import Diffeo
from source.problem import Problem
from source import utils, utils_fn

# mesh = fn.UnitSquareMesh(10, 10)
#
# V = fn.FunctionSpace(mesh, 'P', 1)
# T = fn.VectorFunctionSpace(mesh, 'P', 1)
#
# f = fn.project(fn.Expression('sin(x[0])*cos(x[1])', degree=1), V)
# v = fn.project(fn.grad(f), T)
#
# print(type(v.compute_vertex_values()))
#
# print(np.shape(f.compute_vertex_values()), np.shape(v.compute_vertex_values()))
# # mesh.coordinates()[:] = v.compute_vertex_values()
# print(np.shape(mesh.coordinates()))




# parameters
step_num = 2  # number of time steps
n = 10  # number of mesh elements

# Make image list
sz = 100  # size of images
imgs = utils.make_image_list(sz)


# Make problem class
prob = Problem(imgs, n, step_num, fam='P', deg=1)

f = fn.project(fn.Expression('sin(x[0])*cos(x[1])/100', degree=1), prob.V1)
v = fn.project(fn.grad(f), prob.T1)


# Make diffeomorphism class
diff = Diffeo(prob)

phi = fn.Function(prob.T1)
phi.vector()[:] = v.vector().array()
print(np.max(phi.vector().array()))

diff.compose(f, phi)


fn.plot(f, interactive=True)
