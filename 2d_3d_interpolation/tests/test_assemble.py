import fenics as fn

n = 10

mesh = fn.UnitSquareMesh(n, n)

V = fn.FunctionSpace(mesh, 'P', 1)


f = fn.Function(V)
f.vector().zero()
f = f + 0.01

h = fn.assemble(fn.project(fn.acos(fn.inner(fn.grad(f), fn.grad(f)) + 0.1), V)*fn.dx)

print(type(f), type(h))

print(h)
