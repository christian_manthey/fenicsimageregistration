"""
Test composition with diffeomorphism class.
"""

import fenics as fn
import numpy as np
from source.diffeo import Diffeo
from source.problem import Problem
from source import utils, utils_fn


# parameters
step_num = 2  # number of time steps
n = 100  # number of mesh elements

# Make image list
sz = 100  # size of images
imgs = utils.make_image_list(sz)


# Make problem class
prob = Problem(imgs, n, step_num, fam='P', deg=1)

# Make diffeomorphism class
diff = Diffeo(prob)

# create a function and gradient (velocity)
v = fn.project(fn.Expression("sin(10.0*x[0])*sin(10.0*x[1])", degree=1), prob.V0)
vel = fn.grad(v)/300


vec = fn.project(fn.Expression(('sin(10.0*x[0])*sin(10.0*x[1])', 'sin(x[1])'), degree=1), prob.T0)


# Compose imgs with (x + v(x))
f2 = diff.compose(fn.project(prob.imgs0_fn[0]+prob.imgs0_fn[1], prob.V1), vel)
v2 = diff.compose_vector(vec, vel)


# Show the composition and mesh2
# fn.plot(vec*1.001)
fn.plot(v2*1.001, title='new vector')
fn.plot(f2)
fn.plot(prob.mesh2)
fn.plot(vel)
fn.interactive()
