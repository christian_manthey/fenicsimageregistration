import fenics as fn
import numpy as np

n=100
n2 = 40

mesh0 = fn.UnitSquareMesh(n,n)  # Initial image mesh
mesh1 = fn.UnitSquareMesh(n2,n2)  # mesh for solving PDEs
mesh2 = fn.UnitSquareMesh(n2,n2)  # mesh for moving vertices


""" Define function spaces """
V0 = fn.FunctionSpace(mesh0, 'P', 2)
V1 = fn.FunctionSpace(mesh1, 'P', 1)
V2 = fn.FunctionSpace(mesh2, 'P', 1)
T1 = fn.VectorFunctionSpace(mesh1, 'P', 1)

# Make expression
v = fn.Expression("sin(10.0*x[0])*sin(10.0*x[1])", degree=1)


# Create function on V0 and interpolate v
v0 = fn.Function(V0)
v0.interpolate(v)


# get mesh1's coordinates
mesh1_x = mesh1.coordinates()


# Interpolate v3 onto mesh2
v1 = fn.Function(V1)
v1.interpolate(v0)

v2 = fn.Function(V2)
v2.vector()[:] = v1.vector().array()

# create displacement vector from grad(v1)
vel1 = fn.grad(v1)/200.0

# move mesh coordinates.  Could also: mesh.coordinates()[:] = positions
fn.ALE.move(mesh1, fn.project(vel1, T1))

fn.plot(mesh1, interactive=True)
