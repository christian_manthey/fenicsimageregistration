import fenics as fn
import numpy as np
from matplotlib import pylab as plt
from source import utils, utils_fn
from source.problem import Problem
from source.diffeo import Diffeo
from matplotlib import pylab as plt

# solver.parameters["linear_solver"] = "mumps"
# fn.parameters["num_threads"] = 6

# parameters
step_num = 5  # number of time steps
n = 15  # number of mesh elements

# Make image list
sz = 15  # size of images
imgs = utils.make_image_list_3d(sz)
# plt.figure(), plt.imshow(imgs[0]), plt.pause(100)


# Make problem class
prob = Problem(imgs, n, step_num, fam='P', deg=1)


# Make diffeomorphism class
diff = Diffeo(prob)


# project img0 into V1
img0_v1 = fn.project(prob.imgs0_fn[0], prob.V1)

# For product of img0 and mu_dot_over_mu
h_fn = fn.Function(prob.V1)


print('ok, starting algorithm.')

for a in range(step_num):
    mu_dot_over_mu = diff.compose(prob.mu_dot_over_mu[a], diff.dinv_phi)
    h = mu_dot_over_mu * prob.imgs0_fn[0]
    h_fn.vector()[:] = fn.project(h, prob.V1).vector().array()
    f = utils_fn.solve_2d_diffeo_problem(h_fn, prob.V1)
    v = utils_fn.grad(f, img0_v1, prob.dim)
    diff.update_phi(v)
    # print('The norm of image 0 is: ', fn.assemble(img0_v1*fn.dx))
    # fn.plot(img0_v1, title='Image 0')
    # fn.plot(mu_dot_over_mu, title='mu dot over mu')
    # fn.plot(v, title='velocity')
    # fn.plot(diff.dinv_psi)
    # fn.interactive()

exit()

# Calculate transformations
diff.calc_det_jac()
mu0_transformed = diff.pull_back(prob.imgs0_fn[0])

# Move mesh
diff.move_mesh2(diff.dinv_phi)

# Plot the mesh, transformed density
fn.plot(prob.mu_dot_over_mu[0], title='mu dot over mu')
fn.plot(prob.imgs0_fn[1], title = 'mu 1')
fn.plot(mu0_transformed, title = 'mu 0 tranformed')
fn.plot(prob.mesh2, title='warp')
fn.interactive()



#end
