import fenics as fn


mesh = fn.UnitSquareMesh(10, 10)

# get & print mesh dimensions
dim = mesh.geometry().dim()
print('dimension is: ', dim)


tdim = mesh.topology().dim()

print('tdim is: ', tdim)
