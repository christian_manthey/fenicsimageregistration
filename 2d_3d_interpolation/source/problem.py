"""
Class problem holds meshes, function spaces, dimension, limits etc...
"""
import fenics as fn
import numpy as np
from periodic_boundary import Periodic_boundary_2d, Periodic_boundary_3d
from source import utils
from source import utils_fn



class Problem(object):
    def __init__(self, imgs, n, step_num, fam='P', deg=1):
        """
        imgs: list of two numpy arrays (either 2 or 3D)
        n: number of mesh elements in 1D for mesh2 & mesh3
        step_num: number of time steps
        fam: family
        deg: degree
        """
        self.fam = fam
        self.deg = deg
        self.n = n
        self.step_num = step_num  # number of time steps
        self.dt = 1.0/self.step_num
        self.t = np.linspace(0.0, 1.0, self.step_num)  # times
        self.dim = imgs[0].ndim  # get the array's dimensions
        self.imgs0 = imgs  # list of two images
        self.imgs0_fn = []  # empty list for fenics functions
        self.process_images()


        # Initialize the meshes
        self.init_meshes()

        # Make the periodic boundaries
        self.make_periodic_boundary()

        # Initialize the function spaces (and vector function spaces)
        self.init_functionspaces()

        # Convert images into fenics functions over mesh0.
        self.convert_images()

        # Calculate mesh vertex coordinates
        self.mesh0_x0 = self.mesh0.coordinates()

        # Calculate dof coordinates
        self.dof0_x0 = self.V0.tabulate_dof_coordinates()

        # Calculate mesh0 volume
        self.vol_m0 = self.calculate_manifold_volume()

        # Make limits
        self.lim = utils.make_limits(self.mesh0_x0, self.dim)

        # Normalize images s.t. integral equal manifold volume
        for a in range(2):
            self.imgs0_fn[a] = self.normalize_function(self.imgs0_fn[a])

        # Make mu dot over mu
        self.make_mu_dot_over_mu()

        # Calculate the number of vertices in each mesh
        self.vert0 = len(self.mesh0.coordinates())
        self.vert1 = len(self.mesh1.coordinates())
        self.vert2 = len(self.mesh2.coordinates())


    def make_mu_dot_over_mu(self):
        """
        Make a list of mu dot over mu
        """
        self.mu_dot_over_mu = utils_fn.make_mu_dot_over_mu(self.imgs0_fn,
            self.V0, self.mesh0, self.vol_m0, self.t)


    """ Need to calculate the integral NOT the L2 norm. """
    def calculate_manifold_volume(self):
        id = fn.Function(self.V0)
        id.vector()[:] = 1.0
        vol_m = fn.assemble(id*fn.dx)
        return vol_m


    def normalize_function(self, f):
        """
        Normalizes a Function
        """
        norm = fn.assemble(f*fn.dx)
        vol_m = fn.assemble(f/f*fn.dx)
        f.vector()[:] = f.vector().array() - norm + vol_m
        return f


    def convert_images(self):
        for a in range(2):
            self.imgs0_fn.append(
                utils.convert_numpy_image_to_fenics(self.imgs0[a], self.V0,
                self.mesh0))

    def make_periodic_boundary(self):
        if self.dim ==2:
            self.pb = Periodic_boundary_2d()
        elif self.dim == 3:
            self.pb = Periodic_boundary_3d()


    def init_functionspaces(self):
        self.V0 = fn.FunctionSpace(self.mesh0, self.fam, self.deg)
        self.T0 = fn.VectorFunctionSpace(self.mesh0, self.fam, self.deg)
        self.V1 = fn.FunctionSpace(self.mesh1, self.fam, self.deg,
            constrained_domain=self.pb)
        self.V2 = fn.FunctionSpace(self.mesh2, self.fam, self.deg)
        self.T1 = fn.VectorFunctionSpace(self.mesh1, self.fam, self.deg,
            constrained_domain=self.pb)
        self.T2 = fn.VectorFunctionSpace(self.mesh2, self.fam, self.deg)


    def init_meshes(self):
        if self.dim == 2:
            self.mesh0 = fn.UnitSquareMesh(self.sh0[0], self.sh0[1])  # Image mesh
            self.mesh1 = fn.UnitSquareMesh(self.n, self.n)  # Mesh for solving PDEs
            self.mesh2 = fn.UnitSquareMesh(self.n, self.n)  # Mesh for moving vertices
        elif self.dim ==3:
            self.mesh0 = fn.BoxMesh(fn.Point(0,0,0), fn.Point(1,1,1),
                self.sh0[0], self.sh0[1], self.sh0[2])
            self.mesh1 = fn.BoxMesh(fn.Point(0,0,0), fn.Point(1,1,1), self.n,
                self.n, self.n)
            self.mesh2 = fn.BoxMesh(fn.Point(0,0,0), fn.Point(1,1,1), self.n,
                self.n, self.n)
        else:
            print('Dimension isn\'t 2 or 3 ? ')


    def process_images(self):
        self.check_image_shapes()  # set sh & check image shapes


    def check_image_shapes(self):
        sh = []
        for a in range(len(self.imgs0)):
            sh.append(np.shape(self.imgs0[a]))
        if sh[0] == sh[1]:
            self.sh0 = np.shape(self.imgs0[0])
        else:
            print('The images don\'t have the same dimensions.')
            exit()
