"""
Diffeomorphism class

Things to consider:
    - Determine which functions are defined on which function spaces (mesh1 or mesh 2)


TO DO:
    - Update psi & phi



"""

import fenics as fn
import numpy as np
from source import utils


class Diffeo(object):
    def __init__(self, problem):
        self.prob = problem
        self.epsilon = 1.0/problem.step_num

        # Initialize diffeomorphisms
        self.psi = fn.Function(problem.T1)
        self.inv_psi = fn.Function(problem.T1)
        self.dpsi = fn.Function(problem.T1)
        self.dinv_psi = fn.Function(problem.T1)
        self.phi = fn.Function(problem.T1)
        self.inv_phi = fn.Function(problem.T1)
        self.dphi = fn.Function(problem.T1)
        self.dinv_phi = fn.Function(problem.T1)


    def compose(self, f, vel):
        """
        Compose function with diffeomorphism.
        vel(x) will be changed to within the problem's manifold limits

        f o (x + vel(x))

        f: fenics function in V0 or V1
        vel: fenics Grad object (not function over T1)
        return: fenics function over mesh1
        """
        self.prob.mesh2.coordinates()[:] = self.prob.mesh1.coordinates()

        f2 = fn.Function(self.prob.V2)
        vel2 = fn.Function(self.prob.T2)
        f2 = fn.project(f, self.prob.V2, solver_type='cg')
        vel2 = fn.project(vel, self.prob.T2, solver_type='cg')

        fn.ALE.move(self.prob.mesh2, vel2)

        self.prob.mesh2.coordinates()[:] = utils.change_pos(
            self.prob.mesh2.coordinates(), self.prob.lim, self.prob.dim)

        # Interpolate and set values
        d = fn.Function(self.prob.V2)
        d = fn.project(f, self.prob.V2, solver_type='cg')
        self.prob.mesh2.coordinates()[:] = self.prob.mesh1.coordinates()
        d2 = fn.Function(self.prob.V2)
        d2.vector()[:] = d.vector().array()
        f = fn.project(d2, self.prob.V1, solver_type='cg')

        return f


    def compose_vector(self, v, vel):
        """
        Compose vector (v) with diffeomorphism.
        vel(x) will be changed to within the problem's manifold limits

        v o (x + vel(x))

        v: fenics vector function in T0 or T2
        vel: fenics Grad object (not function over T1)
        return: fenics vector function in T0
        """
        self.prob.mesh2.coordinates()[:] = self.prob.mesh1.coordinates()

        v2 = fn.Function(self.prob.T2)
        vel2 = fn.Function(self.prob.T2)
        v2.interpolate(v)
        vel2.interpolate(vel)


        fn.ALE.move(self.prob.mesh2, vel2)

        self.prob.mesh2.coordinates()[:] = utils.change_pos(self.prob.mesh2.coordinates(), self.prob.lim, self.prob.dim)

        # Interpolate and set values
        d = fn.Function(self.prob.T2)
        d.interpolate(v)
        self.prob.mesh2.coordinates()[:] = self.prob.mesh1.coordinates()
        d2 = fn.Function(self.prob.T2)
        d2.vector()[:] = d.vector().array()
        v = fn.Function(self.prob.T1)
        v.interpolate(d2)

        # fn.ALE.move(self.prob.mesh2, vel2)
        #
        # self.prob.mesh2.coordinates()[:] = self.prob.mesh1.coordinates()


        return v

    def update_psi(self, vel):
        """
        Update psi & inverse psi (as fenics vector functions).
        """

        # self.psi.vector()[:] = 1.0 + \
        #     self.prob.dt * fn.project(vel, self.prob.T1).vector().array()
        # self.psi.vector()[:] = 1.0 - \
        #     self.prob.dt * fn.project(vel, self.prob.T1).vector().array()

        # Update d psi & d inverse psi
        self.dpsi.vector()[:] = self.epsilon * \
            fn.project(vel, self.prob.T1, solver_type='cg').vector().array()
        self.dinv_psi.vector()[:] = - self.epsilon * \
            fn.project(vel, self.prob.T1, solver_type='cg').vector().array()


    def update_phi(self, vel):

        # Update psi first
        self.update_psi(vel)

        self.dinv_phi.vector()[:] = self.compose_vector(self.dinv_phi,
            self.dinv_psi).vector().array() - \
            self.epsilon * fn.project(vel, self.prob.T1, solver_type='cg').vector().array()

        self.dphi.vector()[:] = self.compose_vector(self.dpsi,
            self.dphi).vector().array() + self.dphi.vector().array()

        # Calculate: phi = psi o phi  & inv phi = inv phi o inv psi
        # self.phi.vector()[:] = self.compose_vector(self.psi,
        #     self.dphi).vector().array()
        # self.inv_phi.vector()[:] = self.compose_vector(self.inv_phi,
        #     self.dinv_psi).vector().array()

    def calc_det_jac(self):
        """
        Calculate the det(Dphi)
        """

        id = fn.Identity(self.prob.dim)
        self.det_jac_phi = fn.det(fn.grad(self.dphi) + id)
        self.det_jac_inv_phi = fn.det(fn.grad(self.dinv_phi) + id)


    def pull_back(self, mu0):
        """
        Convert mu0 -> mu1 through pushforward transformation.

        :param mu0: initial density, numpy array with shape: ( , )
        :param x0: initial positions of mu0
        :return:
        """
        mu = fn.Function(self.prob.V1)
        mu = self.compose(mu0, self.dphi)

        self.mu0_transformed = fn.project(self.det_jac_phi * mu, self.prob.V1, solver_type='cg')

        mu_v0 = fn.Function(self.prob.V1)

        mu_v0.interpolate(self.mu0_transformed)

        return mu_v0


    def move_mesh2(self, dphi):
        """
        Move mesh 2

        """

        # Return mesh2 coordinates to original positions
        self.prob.mesh2.coordinates()[:] = self.prob.mesh1.coordinates()


        dphi = fn.project(dphi, self.prob.T2, solver_type='cg')

        # Move the mesh
        fn.ALE.move(self.prob.mesh2, dphi)
