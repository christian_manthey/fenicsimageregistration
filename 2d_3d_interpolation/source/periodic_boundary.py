import fenics as fn



class Periodic_boundary_2d(fn.SubDomain):
    def inside(self, x, on_boundary):
        return bool((fn.near(x[0], 0) or fn.near(x[1], 0)) and
                    (not ((fn.near(x[0], 0) and fn.near(x[1], 1)) or
                          (fn.near(x[0], 1) and fn.near(x[1], 0))))
                    and on_boundary)

    def map(self, x, y):
        if fn.near(x[0], 1.0) and fn.near(x[1], 1.0):
            y[0] = x[0] - 1.0
            y[1] = x[1] - 1.0
        elif fn.near(x[0], 1.0):
            y[0] = x[0] - 1.0
            y[1] = x[1]
        else:
            y[0] = x[0]
            y[1] = x[1] - 1.0


""" Need to finish this. """
class Periodic_boundary_3d(fn.SubDomain):
    def inside(self, x, on_boundary):
        return bool((fn.near(x[0], 0) or fn.near(x[1], 0) or fn.near(x[2], 0))
            and (not (
            (fn.near(x[0], 1) and fn.near(x[1], 0) and fn.near(x[2], 0))
            or (fn.near(x[0], 0) and fn.near(x[1], 1) and fn.near(x[2], 0))
            or (fn.near(x[0], 0) and fn.near(x[1], 0) and fn.near(x[2], 1)))
            ) and on_boundary)

    def map(self, x, y):
        if fn.near(x[0], 1.0) and fn.near(x[1], 1.0) and fn.near(x[2], 1.0):
            y[0] = x[0] - 1.0
            y[1] = x[1] - 1.0
            y[2] = x[2] - 1.0
        elif fn.near(x[0], 1.0):
            y[0] = x[0] - 1.0
            y[1] = x[1]
            y[2] = x[2]
        elif fn.near(x[1], 1.0):
            y[0] = x[0]
            y[1] = x[1] - 1.0
            y[2] = x[2]
        else:
            y[0] = x[0]
            y[1] = x[1]
            y[2] = x[2] - 1.0
