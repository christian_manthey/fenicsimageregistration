"""
Utility functions.
"""
import fenics as fn
import numpy as np
import scipy.interpolate
import scipy.ndimage
from scipy import misc
import os
from matplotlib import pylab as plt


def make_image_list_3d(sz):
    # Make two (3D) numpy arrays
    gaussian_std = 1.0
    bg_den = 0.2  # background density
    mx_den = 2.0

    # cwd = os.getcwd()
    # imgj = misc.imread(cwd + r'/fig/j.png')
    # imgv = misc.imread(cwd + r'/fig/v.png')

    imgs = make_image_list2(sz)
    imgj = imgs[0]
    imgv = imgs[1]


    # plt.figure(), plt.imshow(imgj), plt.colorbar()

    imgj = misc.imresize(imgj, (sz,sz))
    imgv = misc.imresize(imgv, (sz,sz))

    imgj = imgj - np.min(imgj)
    imgj = (mx_den- bg_den)*imgj/np.max(imgj) + bg_den

    imgv = imgv - np.min(imgv)
    imgv = (mx_den-bg_den)*imgv/np.max(imgv) + bg_den

    # plt.figure(), plt.imshow(imgj), plt.colorbar()
    #
    # print(imgj.shape)


    img0 = bg_den * np.ones((sz, sz, sz), dtype='float')
    img1 = bg_den * np.ones((sz, sz, sz), dtype='float')

    img0[sz//2-2:sz//2+2, :, :] = imgj
    img1[sz//2-2:sz//2+2, :, :] = imgv


    # plt.figure(), plt.imshow(np.squeeze(img1[sz//2,:,:])), plt.colorbar(), plt.pause(100)
    # exit()

    img0 = scipy.ndimage.filters.gaussian_filter(img0, gaussian_std)
    img1 = scipy.ndimage.filters.gaussian_filter(img1, gaussian_std)


    # print(np.min(img0), np.max(img0), img0.shape)
    # exit()

    # # print(img1.shape)
    # # exit()
    #
    # plt.figure(2), plt.imshow(imgj)
    # plt.figure(1), plt.imshow(img0[sz//2,:,:]), plt.pause(100)
    # exit()

    # Make list of images
    imgs = [img0, img1]

    return imgs

def make_image_list2(sz):
    # Make two (2D) numpy arrays
    gaussian_std = 2.0
    bg_den = 0.6  # background density

    # Load images
    cwd = os.getcwd()
    img_j = misc.imread(cwd + r'/fig/j.png')
    img_v = misc.imread(cwd + r'/fig/v.png')
    img0 = np.sum(img_j, axis=2)
    img1 = np.sum(img_v, axis=2)
    img0 = img0 - np.min(img0)
    img0 = 0.8*img0/np.max(img0) + bg_den
    img1 = img1 - np.min(img1)
    img1 = 0.8 * img1 / np.max(img1) + bg_den
    img0 = np.flipud(img0)
    img1 = np.flipud(img1)

    # Make list of images
    imgs = [img0, img1]

    return imgs







def make_image_list(sz):
    # Make two (2D) numpy arrays
    gaussian_std = 2.0
    bg_den = 0.2  # background density
    mx_den = 4.0

    # Make image by hand
    # x11, x12 = (10, 35)  # first image start and end points (horizontally)
    # x21, x22 = (20, 60)
    # y11, y12 = (30, 75)  # second image start and end points (horizontall)
    # y21, y22 = (40, 50)
    #
    # img0 = bg_den * np.ones((sz, sz), dtype='float')
    # img0[y11:y12, x11:x12] = 1.0
    # img1 = bg_den * np.ones((sz, sz), dtype='float')
    # img1[y21:y22, x21:x22] = 1.0


    # Load images
    cwd = os.getcwd()
    img_j = misc.imread(cwd + r'/fig/j.png')
    img_v = misc.imread(cwd + r'/fig/v.png')
    img0 = np.sum(img_j, axis=2)
    img1 = np.sum(img_v, axis=2)
    img0 = img0 - np.min(img0)
    img0 = (mx_den-bg_den)*img0/np.max(img0) + bg_den
    img1 = img1 - np.min(img1)
    img1 = (mx_den-bg_den) * img1 / np.max(img1) + bg_den
    img0 = np.flipud(img0)
    img1 = np.flipud(img1)

    img0 = scipy.ndimage.filters.gaussian_filter(img0, gaussian_std)
    img1 = scipy.ndimage.filters.gaussian_filter(img1, gaussian_std)


    # Make list of images
    imgs = [img0, img1]

    return imgs


def convert_numpy_image_to_fenics(img, V, mesh):
    """
    Convert initial numpy arrays (image densities) into fenics functions.
    """
    dim = img.ndim
    sh = img.shape
    if dim == 2:
        img_fn = convert_2d_image(img, V, mesh)
    elif dim == 3:
        img_fn = convert_3d_image(img, V, mesh)
    return img_fn

def convert_3d_image(img, V, mesh):
    """
    Outline
        Get dof coordinates
        Make pixel coordinates (between [0,1]x[0,1])
        Interpolate image at dof coordinates.

        ONLY FOR INITIAL IMAGES

    :param img:
    :param V:
    :return:
    """
    dim = 3
    sh = img.shape
    img = img.flatten()
    # N = int(len(img)**(1.0/3.0))
    N = sh[0]
    x0 = mesh.coordinates()
    img_fn = fn.Function(V)
    dof_x = V.tabulate_dof_coordinates().reshape((-1, dim))  # dof positions
    lim = make_limits(x0, dim)
    img_x = make_initial_positions(N, dim, lim)
    # print(np.shape(img), np.shape(img_x), np.shape(dof_x), N)
    # exit()
    img2 = calc_new_density(img, img_x, dof_x)
    img_fn.vector()[:] = img2[:]
    return img_fn



def convert_2d_image(img, V, mesh):
    """
    Outline
        Get dof coordinates
        Make pixel coordinates (between [0,1]x[0,1])
        Interpolate image at dof coordinates.

        ONLY FOR INITIAL IMAGES

    :param img:
    :param V:
    :return:
    """
    img = img.flatten()
    N = int(len(img)**0.5)
    x0 = mesh.coordinates()
    dim = np.ndim(x0)
    img_fn = fn.Function(V)
    dof_x = V.tabulate_dof_coordinates().reshape((-1, dim))  # dof positions
    lim = make_limits(x0, dim)
    img_x = make_initial_positions(N, dim, lim)
    img2 = calc_new_density(img, img_x, dof_x)
    img_fn.vector()[:] = img2[:]
    return img_fn

def make_limits(x0, dim):
    """
    lim: [min/max, dim]
    """
    lim = np.zeros((2, dim), dtype='float')
    for a in range(dim):
        lim[0, a] = np.min(x0[:, 0])
        lim[1, a] = np.max(x0[:, 0])
    return lim


def make_initial_positions(N, dim, lim):
    x1 = np.zeros((N, dim), dtype='float')
    for a in range(dim):
        x1[:, a] = np.linspace(lim[0, 0], lim[1, 0], N)
    if dim == 2:
        x2, y2 = np.meshgrid(x1[:, 0], x1[:, 1])
        x = np.zeros((N ** dim, 2), dtype='float')
        x[:, 0] = x2.flatten()
        x[:, 1] = y2.flatten()
    elif dim == 3:
        x2, y2, z2 = np.meshgrid(x1[:, 0], x1[:, 1], x1[:, 2])
        x = np.zeros((N ** dim, 3), dtype='float')
        x[:, 0] = x2.flatten()
        x[:, 1] = y2.flatten()
        x[:, 2] = z2.flatten()
        # print(x.shape)
    return x

def calc_new_density(img, x0, x, mthd='linear'):
    """
    Calculate a new image.
    img: image
    x: new positions (N**2, 2)
    mthd: interpolation method (nearest, linear, cubic)
    """
    img2 = scipy.interpolate.griddata(x0, img.flatten(),
                                      x, method=mthd,
                                      fill_value='Nan')
    return img2

def change_pos(x, lim, dim):
    """
    Make sure coordinates stay within the T^2 (2D torus) s.t. phi: M -> M

    :param x: positions
    :param epsilon:
    :param vel: velocity
    :param lim:
    :return: New positions that fall withing T^2 (2D torus)
    """


    d = np.zeros_like(x)
    for a in range(dim):
        v1 = x[:, a] < lim[0, a]
        v2 = x[:, a] > lim[1, a]
        d[v1, a] = np.abs(x[v1, a] - lim[0, a])
        d[v2, a] = np.abs(x[v2, a] - lim[0, a])
        l = lim[1, a] - lim[0, a]
        n = d/l
        r = n % 1.0  # Remove the integer values
        x[v1, a] = lim[1, a] - r[v1, a]
        x[v2, a] = lim[0, a] + r[v2, a]





    # for a in range(dim):
    #     v1 = x[:, a] < lim[0, a]
    #     v2 = x[:, a] > lim[1, a]
    #     d = lim[1, a] - lim[0, a]
    #     x[v1, a] = x[v1, a] + d
    #     x[v2, a] = x[v2, a] - d

    # print('min and max positions are: ', np.min(x), np.max(x))

    return x
