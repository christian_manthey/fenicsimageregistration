"""
Utility functions for fenics manipulations.
"""

import fenics as fn


def make_mu_dot_over_mu(imgs, V, mesh, vol_m, t):
    """
    Make mu dot/mu for algorithm 1.
    """
    img0 = imgs[0]
    img1 = imgs[1]

    f0, f1 = fn.sqrt(imgs[0]), fn.sqrt(imgs[1])

    f01_norm = fn.assemble(f0 * f1 * fn.dx)

    theta = fn.acos(1.0/vol_m * f01_norm)

    mu_dot_over_mu = []
    for a in t:
        numerator = 2.0 * theta * (fn.cos(theta - a * theta) * f0 - fn.cos(a * theta) * f1)
        denominator = f1 * fn.sin(a * theta) + fn.sin(theta - a * theta) * f0
        mu_dot_over_mu.append(fn.project(numerator / denominator, V, solver_type='cg'))

    return mu_dot_over_mu



def grad(f, img, dim):
    """
    Calculates a gradient with a non-euclidean metric tensor (g).

    see: 'Diffeomorphic density matching by optimal information transport' by Martin Bauer et. al.
    Appendix: A.1

    g = img^(2/dim) * identity

    return: fenics Grad object (or Product)

    T: Vector function space
    """

    grad_f = img**(-dim/2)*fn.grad(f)
    return grad_f


def assemble_a_poisson(img0, V):
    """

    Assembles a(u,v) for the poisson equation.

    Returns A(u,v)

    """
    dim = img0.geometric_dimension()

    f = fn.TrialFunction(V)
    v = fn.TestFunction(V)

    a = fn.inner(fn.grad(v), fn.grad(f)) * fn.dx

    A = fn.assemble(a)

    return A


def solve_poisson(img, V, img0, A=None):
    """
    Solve equation 2 from algorithm 1.

    nabla(f) = img_k

    """
    dim = img.geometric_dimension()

    v = fn.TestFunction(V)
    L = img * v * fn.dx
    u = fn.Function(V)

    if A is None:
        f = fn.TrialFunction(V)
        a = img0**(1.0 - dim/2.0) * fn.inner(fn.grad(v), fn.grad(f)) * fn.dx
        fn.solve(a == L, u, 'cg')
    else:
        #assemble L and solve equation
        b = fn.assemble(L)
        fn.solve(A, u.vector(), b, 'cg')

    return u

def solve_diffeo_problem(img, V, img00, A=None):
    """
    Solve div(grad(f)) = I

    :param img: fenics function
    :param V:
    :return:
    """

    # Make sum(I) = 0
    norm = fn.assemble(img * fn.dx)
    img = img - norm
    u = solve_poisson(img, V, img00, A=A)
    return u
