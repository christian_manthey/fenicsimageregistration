"""
For testing the algorithm.
"""

import fenics as fn
import numpy as np
import scipy as sci
from scipy import misc
import scipy.ndimage
from source.utils import convert_image, make_limits, make_mu_dot_over_mu_imgs, normalize_image, interp_and_convert_image
from source.utils import make_image_positions, solve_2d_poisson, solve_2d_diffeo_problem, convert_function_to_vector, convert_vector_to_function
from source.utils import move_mesh, interpolate_image_new_positions, calc_new_density, flatten_np_for_fn
from source.diffeo import Diffeo
from matplotlib import pylab as plt
from source.utils import save_mesh_fig, grad
import os



def init_images(sz):
    gaussian_std = 2.0

    x11, x12 = (10, 35)  # first image start and end points (horizontally)
    x21, x22 = (20, 60)
    y11, y12 = (30, 75)  # second image start and end points (horizontall)
    y21, y22 = (40, 50)

    bg_den = 0.2
    img0 = bg_den * np.ones((sz, sz), dtype='float')
    img0[y11:y12, x11:x12] = 1.0
    img1 = bg_den * np.ones((sz, sz), dtype='float')
    img1[y21:y22, x21:x22] = 1.0

    # Load images
    cwd = os.getcwd()
    img_j = misc.imread(cwd + r'/fig/j.png')
    img_v = misc.imread(cwd + r'/fig/v.png')
    img0 = np.sum(img_j, axis=2)
    img1 = np.sum(img_v, axis=2)
    img0 = img0 - np.min(img0)
    img0 = 0.8*img0/np.max(img0) + bg_den
    img1 = img1 - np.min(img1)
    img1 = 0.8 * img1 / np.max(img1) + bg_den
    img0 = np.flipud(img0)
    img1 = np.flipud(img1)
    img0 = scipy.ndimage.filters.gaussian_filter(img0, gaussian_std)
    img1 = scipy.ndimage.filters.gaussian_filter(img1, gaussian_std)


    # Make the first image a constant
    # img0 = bg_den * np.ones_like(img1)

    return(img0, img1)


class Periodic_boundary(fn.SubDomain):
    def inside(self, x, on_boundary):
        return bool((fn.near(x[0], 0) or fn.near(x[1], 0)) and
                    (not ((fn.near(x[0], 0) and fn.near(x[1], 1)) or
                          (fn.near(x[0], 1) and fn.near(x[1], 0))))
                    and on_boundary)

    def map(self, x, y):
        if fn.near(x[0], 1.0) and fn.near(x[1], 1.0):
            y[0] = x[0] - 1.0
            y[1] = x[1] - 1.0
        elif fn.near(x[0], 1.0):
            y[0] = x[0] - 1.0
            y[1] = x[1]
        else:
            y[0] = x[0]
            y[1] = x[1] - 1.0


def init_finite_element(n, dg):

    mesh = fn.UnitSquareMesh(n, n)

    pb = Periodic_boundary()

    V = fn.FunctionSpace(mesh, 'P', dg,
                         constrained_domain=pb)
    T = fn.VectorFunctionSpace(mesh, 'P', dg,
                                constrained_domain=pb)  # vector function space
    return (V, T, mesh)


def test():

    sz = 80  # Number of pixels in image along one direction (No longer used)
    dg = 1  # function space degree
    n = 100  # number of FE cells in one direction
    dim = 2  # dimension (2 or 3)
    step_num = 10  # number of time steps
    dt = 1.0/step_num
    t = np.linspace(0.0, 1.0, step_num)  # time varies from 0.0 to 1.0

    # Initialize the images and finite element variables
    V, T, mesh = init_finite_element(n, dg)
    img0, img1 = init_images(sz)
    sz = np.shape(img0)[0]

    # dof positions
    dof_x0 = V.tabulate_dof_coordinates().reshape((-1, dim))

    # Get image pixel positions
    img0_x0 = make_image_positions(img0, mesh)

    # mesh vertices
    mesh_x0 = mesh.coordinates()

    # Make limits
    lim = make_limits(dof_x0, dim)
    lim_mesh = make_limits(mesh_x0, dim)

    # Normalize image integrals equal to manifold volume
    img0, img1 = normalize_image(img0, lim_mesh), normalize_image(img1, lim_mesh)

    plt.figure(), plt.imshow(img0 - img1), plt.colorbar(), plt.pause(1)

    # Convert the images into FEniCS functions
    img0_fn = convert_image(img0, V, mesh)
    img1_fn = convert_image(img1, V, mesh)
    fn.plot(img1_fn-img0_fn, interactive=False, title='img1 - img0')

    # Make mu dot/ mu in images
    h_img = make_mu_dot_over_mu_imgs(img0, img1, t, lim)

    # Show the first mu dot/mu image
    h1 = convert_image(h_img[0], V, mesh)
    fn.plot(h1, title='First mu dot/mu')

    # Create diffeomorphism
    phi = Diffeo(dof_x0, lim, V, T, mesh, dim)


    """
    Notes:
        Mesh vertices -> outside of M (T^2), so interpolation fails.  (phi: M -> N)
            Update displacements instead.

        Bug: h1 = calc_new_density(...)
            Use interp_and_convert(...) NOT convert_image

        Bug: problem with phi_dof_x on the edges due to periodic boundaries
            dof_x -> periodic -> jacobian large on the edges

    """

    """ Will need to make conformal metric for non-constant initial image. """

    h_fn = fn.Function(V)
    for a in range(len(h_img)):
        h1 = calc_new_density(h_img[a], img0_x0, phi.inv_phi_dof_xm)  # Compose mu dot/mu with inverse phi
        h10 = calc_new_density(img0, img0_x0, phi.dof_x0)
        h2 = h1 * h10
        h_fn.vector()[:] = h2  # convert to fenics function
        u = solve_2d_diffeo_problem(h_fn, V)  # solve 2D Poisson eq
        # vel = fn.grad(u)  # Calculate the velocity
        vel = grad(u, img0_fn, 2)
        phi.update_psi(vel, dt, T, mesh)  # Update Psi
        phi.update_phi()  # Update Phi


    fn.plot(vel, interactive=False)

    x_new = mesh.coordinates()
    phi.calc_phi_mesh_from_displacements()
    # phi.calc_phi_dof_from_displacements()  # something wrong with this line
    phi.calc_det_jac()



    fn.plot(phi.d_inv_phi_fn*(1+1e-8), title='d inverse phi')
    fn.plot(phi.det_jac_phi / 1.001, title='det jacobian')
    fn.plot(phi.det_jac_inv_phi / 1.001, title='Inverse det jacobian')
    fn.plot(phi.inv_phi_fn / 1.001, interactive=False, title='Inverse phi')

    # transform mu0 into mu1
    mu1 = phi.pull_back(img0, img0_x0)
    fn.plot(mu1, interactive=False, title='Transformed mu0')

    # Show the warp
    x_new[:] = phi.inv_phi_mesh_x.copy()
    fn.plot(mesh, interactive=True, title='Warp')

    # Show the inverse warp
    # x_new[:] = phi.phi_mesh_x.copy()
    # fn.plot(mesh, interactive=True, title='Inverse warp')

    # Save the warp
    # save_mesh_fig(mesh, dpi=150, sz=(16,12), filename='mesh_coarse')



if __name__ == '__main__':
    test()
