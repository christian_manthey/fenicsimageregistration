import fenics as fn
import numpy as np
import scipy as sci
import scipy.ndimage
from source.utils import convert_image, make_limits, make_mu_dot_over_mu_imgs, normalize_image


def init_images(sz):
    gaussian_std = 4.0

    x11, x12 = (10, 35)  # first image start and end points (horizontally)
    x21, x22 = (20, 60)
    y11, y12 = (30, 75)  # second image start and end points (horizontall)
    y21, y22 = (40, 50)

    img0 = np.ones((sz, sz), dtype='float')
    img0[y11:y12, x11:x12] = 2.2
    img1 = np.ones((sz, sz), dtype='float')
    img1[y21:y22, x21:x22] = 2.0
    img0 = scipy.ndimage.filters.gaussian_filter(img0, gaussian_std)
    img1 = scipy.ndimage.filters.gaussian_filter(img1, gaussian_std)

    return(img0, img1)


class Periodic_boundary(fn.SubDomain):
    def inside(self, x, on_boundary):
        return bool((fn.near(x[0], 0) or fn.near(x[1], 0)) and
                    (not ((fn.near(x[0], 0) and fn.near(x[1], 1)) or
                          (fn.near(x[0], 1) and fn.near(x[1], 0))))
                    and on_boundary)

    def map(self, x, y):
        if fn.near(x[0], 1.0) and fn.near(x[1], 1.0):
            y[0] = x[0] - 1.0
            y[1] = x[1] - 1.0
        elif fn.near(x[0], 1.0):
            y[0] = x[0] - 1.0
            y[1] = x[1]
        else:
            y[0] = x[0]
            y[1] = x[1] - 1.0


def init_finite_element(n, dg):
    """
    Initialize finit element formulation.
    """
    mesh = fn.UnitSquareMesh(n, n)
    pb = Periodic_boundary()
    V = fn.FunctionSpace(mesh, 'P', dg,
                         constrained_domain=pb)
    return (V, mesh)


def test():

    sz = 100  # Number of pixels in image along one direction
    dg = 1  # function space degree
    n = 50  # number of FE cells in one direction
    dim = 2  # dimension (2 or 3)
    dt = 0.1  # time step
    t = np.linspace(0.0, 1.0, 1.0//dt)  # time varies from 0.0 to 1.0

    # Initialize the images and finite element variables
    V, mesh = init_finite_element(n, dg)
    img0, img1 = init_images(sz)


    # mesh vertices
    mesh_x0 = mesh.coordinates()

    # Make limits
    lim = make_limits(mesh_x0, dim)

    # Normalize image integrals equal to manifold volume
    img0, img1 = normalize_image(img0, lim), normalize_image(img1, lim)

    # Convert the images into FEniCS functions
    img0_fn = convert_image(img0, V, mesh)
    img1_fn = convert_image(img1, V, mesh)

    # Make mu dot/ mu in images
    h_img = make_mu_dot_over_mu_imgs(img0, img1, t, lim)

    # Show all the mu dot/ mu
    for a in range(len(h_img)):
        temp = convert_image(h_img[a], V, mesh)
        if a == len(h_img)-1:
            fn.plot(temp, interactive=True)
        else:
            fn.plot(temp, interactive=False)

    fn.plot(img0_fn - img1_fn, interactive=True)  # show the first image - second image


if __name__ == '__main__':
    test()
