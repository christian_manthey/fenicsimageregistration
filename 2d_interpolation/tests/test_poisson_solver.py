import fenics as fn
import scipy.ndimage
import numpy as np
from source.utils import convert_image, solve_2d_poisson

sz = 100
n = 100  # number of mesh vertices

# Make images
img0 = np.ones((sz, sz), dtype='float')
img0[30:75, 10:35] = 2.2
img1 = np.ones((sz, sz), dtype='float')
img1[20:30, 20:60] = 2.0
img0 = scipy.ndimage.filters.gaussian_filter(img0, 2)
img1 = scipy.ndimage.filters.gaussian_filter(img1, 2)


dg = 1
dim = 2
mesh = fn.UnitSquareMesh(n, n)
mn = 0.0
mx = 1.0

class Periodic_boundary(fn.SubDomain):
    def inside(self, x, on_boundary):
        return bool((fn.near(x[0], mn) or fn.near(x[1], mn)) and
                    (not ((fn.near(x[0], mn) and fn.near(x[1], mx)) or
                          (fn.near(x[0], mx) and fn.near(x[1], mn))))
                    and on_boundary)

    def map(self, x, y):
        if fn.near(x[0], mx) and fn.near(x[1], mx):
            y[0] = x[0] - mx
            y[1] = x[1] - mx
        elif fn.near(x[0], mx):
            y[0] = x[0] - mx
            y[1] = x[1]
        else:
            y[0] = x[0]
            y[1] = x[1] - mx

pb = Periodic_boundary()

V = fn.FunctionSpace(mesh, 'P', dg,
                     constrained_domain=pb)


img0_fn = convert_image(img0, V, mesh)

# Need to send I^2 into solver (also make I^2 sum to zero)
img0_squared1 = img0_fn.vector().array()**2
img0_squared1 = img0_squared1 - np.mean(img0_squared1)
img0_squared = fn.Function(V)
img0_squared.vector()[:] = img0_squared1[:]

print(np.sum(img0_fn.vector().array()))

u = solve_2d_poisson(img0_squared, V)


fn.plot(img0_fn, interactive=False)

fn.plot(u, interactive=True)


