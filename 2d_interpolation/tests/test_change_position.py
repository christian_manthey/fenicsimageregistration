"""
Test the change of position utility functions
"""

import fenics as fn
import scipy.ndimage
import numpy as np
from source.utils import convert_image, get_dof_positions, make_limits, change_pos, interp_and_convert_image
from source.utils import make_image_positions, solve_2d_poisson

sz = 100
n = 100  # number of mesh vertices

# Make images
img0 = np.ones((sz, sz), dtype='float')
img0[10:85, 10:30] = 2.2
img1 = np.ones((sz, sz), dtype='float')
img1[20:30, 20:60] = 2.0
img0 = scipy.ndimage.filters.gaussian_filter(img0, 2)
img1 = scipy.ndimage.filters.gaussian_filter(img1, 2)


dg = 1
dim = 2
mesh = fn.UnitSquareMesh(n, n)
mn = 0.0
mx = 1.0

class Periodic_boundary(fn.SubDomain):
    def inside(self, x, on_boundary):
        return bool((fn.near(x[0], mn) or fn.near(x[1], mn)) and
                    (not ((fn.near(x[0], mn) and fn.near(x[1], mx)) or
                          (fn.near(x[0], mx) and fn.near(x[1], mn))))
                    and on_boundary)

    def map(self, x, y):
        if fn.near(x[0], mx) and fn.near(x[1], mx):
            y[0] = x[0] - mx
            y[1] = x[1] - mx
        elif fn.near(x[0], mx):
            y[0] = x[0] - mx
            y[1] = x[1]
        else:
            y[0] = x[0]
            y[1] = x[1] - mx

pb = Periodic_boundary()

V = fn.FunctionSpace(mesh, 'P', dg,
                     constrained_domain=pb)

img0_fn = convert_image(img0, V, mesh)


img0_x0 = make_image_positions(img0, mesh)

# get dof positions
dof_x0 = get_dof_positions(V, dim)
mesh_x0 = mesh.coordinates()

# change the positions
dof_x = np.zeros_like(dof_x0)
dof_x[:, 0] = dof_x0[:, 0] + np.sin(dof_x0[:, 0])**2
dof_x[:, 1] = dof_x0[:, 1] - 0.6*np.cos(dof_x0[:, 1])

# get the limits
lim = make_limits(mesh_x0, dim)

# make sure the positions fall within the 2D torus manifold  psi: T^2 -> T^2
dof_x = change_pos(dof_x, lim)

# interpolate image and convert to FEniCS function
img2 = interp_and_convert_image(img0, img0_x0, dof_x, V, mesh)

# Show the new image
fn.plot(img2, interactive=True)





