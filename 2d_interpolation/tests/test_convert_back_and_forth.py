import fenics as fn
import scipy.ndimage
import numpy as np
from source.utils import convert_image, convert_function_to_vector, convert_vector_to_function
from matplotlib import pylab as plt

sz = 100
n = 50  # number of mesh vertices

# Make images
img0 = np.ones((sz, sz), dtype='float')
img0[30:75, 10:35] = 2.2
img1 = np.ones((sz, sz), dtype='float')
img1[20:30, 20:60] = 2.0
img0 = scipy.ndimage.filters.gaussian_filter(img0, 2)
img1 = scipy.ndimage.filters.gaussian_filter(img1, 2)


dg = 1
dim = 2
mesh = fn.UnitSquareMesh(n, n)


class Periodic_boundary(fn.SubDomain):
    def inside(self, x, on_boundary):
        return bool((fn.near(x[0], 0) or fn.near(x[1], 0)) and
                    (not ((fn.near(x[0], 0) and fn.near(x[1], 1)) or
                          (fn.near(x[0], 1) and fn.near(x[1], 0))))
                    and on_boundary)

    def map(self, x, y):
        if fn.near(x[0], 1.0) and fn.near(x[1], 1.0):
            y[0] = x[0] - 1.0
            y[1] = x[1] - 1.0
        elif fn.near(x[0], 1.0):
            y[0] = x[0] - 1.0
            y[1] = x[1]
        else:
            y[0] = x[0]
            y[1] = x[1] - 1.0

pb = Periodic_boundary()

V = fn.FunctionSpace(mesh, 'P', dg,
                     constrained_domain=pb)


img0_fn = convert_image(img0, V, mesh)

img0_vect = convert_function_to_vector(img0_fn, V)

N = int(np.shape(img0_vect)[0]**0.5)
# plt.figure(), plt.imshow(img0_vect.reshape((N,N))), plt.pause(10)


img1_fn = convert_vector_to_function(img0_vect, V)

fn.plot(img0_fn)
fn.plot(img1_fn, interactive=True)

