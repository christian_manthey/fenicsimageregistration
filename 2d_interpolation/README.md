# README
Author: Erik Malm

Project name: Diffeomorphic density matching (2D with interpolation)

# Overview

Determine the diffeomorphism which transforms one density into the other in 2 dimensions.


# References

See: "Diffeomophic density matching by optimal information transport"

  - By: Martin Bauer et. al. (Klas Modin)

  - Algorithm 1


# Potential future work

Extend to 3D.

Algorithm 2 (2 & 3D)

  - Inexact matching with gradient flow.
