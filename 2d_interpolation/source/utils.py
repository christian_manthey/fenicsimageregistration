"""

Utilities for 2D diffeomorphic density matching.

"""


import fenics as fn
import numpy as np
import scipy.interpolate
import ufl
import matplotlib.pylab as plt

def flatten_np_for_fn(v):
    """
    Flattens numpy array (vector with shape: (N,2)) into format for replacing
    fenics vector.
    """

    n1, n2 = np.shape(v)
    n_tot = int(n1*n2)
    v2 = np.zeros((n_tot,), dtype='float')
    v2[::2] = v[:,0]
    v2[1::2] = v[:,1]

    return v2


def fn_to_np_at_mesh_vert(f, V, mesh):
    """
    Convert fenics Grad to numpy array evaluated at mesh vertices.
    :param f:
    :param V:
    :param mesh:
    :return:
    """
    if (isinstance(f, ufl.differentiation.Grad) or
    isinstance(f, ufl.tensors.ComponentTensor)):

        v = fn.project(f, V)
        v1_mesh = v.sub(0).compute_vertex_values(mesh)
        v2_mesh = v.sub(1).compute_vertex_values(mesh)
        v = np.zeros((len(v1_mesh), 2))
        v[:, 0] = v1_mesh
        v[:, 1] = v2_mesh

        return v


def fn_to_np(f, V, components=None):
    """

    :param f: function, vector, grad
    :param V: function space or vector function space
    :param components: number of components (function: 1, vector: 2...)
    :return:
    """

    if isinstance(f, fn.Function):  # Is this a fenics function?
        if components is None:
            np_f = convert_function_to_vector(f, V)
        elif components == 2:  # then its a vector
            np_f = convert_fn_vector_np_vector(f)

    elif isinstance(f, ufl.differentiation.Grad) or isinstance(f, ufl.tensors.ComponentTensor):  # Is f a grad(u)
        v = fn.project(f, V)
        np_f = convert_fn_vector_np_vector(v)

    return np_f


def convert_fn_vector_np_vector(fn_v):

    lngth = int(len(fn_v.vector().array())/2)
    v = np.zeros((lngth, 2), dtype='float')
    fn_v1 = fn_v.vector().array()
    v[:, 0] = fn_v1[::2]
    v[:, 1] = fn_v1[1::2]

    return v


def array_dof_to_vertices(v, mesh, dim, V=None, T=None):

    mesh_x0 = mesh.coordinates()
    dof_x0 = V.tabulate_dof_coordinates().reshape((-1, dim))
    sh = np.shape(v)
    dim = v.ndim  # dimension
    if dim == 1:  # function
        v2 = interpolate_image_new_positions(v,dof_x0, mesh_x0)
        return v2
    if dim == 2: # vector
        v21 = interpolate_image_new_positions(v[:,0], dof_x0, mesh_x0)
        v22 = interpolate_image_new_positions(v[:,1], dof_x0, mesh_x0)
        v2 = np.zeros((len(v21), 2), dtype='float')
        v2[:,0] = v21.copy()
        v2[:,1] = v22.copy()

        return v2


def move_mesh(mesh, T, vel, epsilon, x=None):
    """
    Moves the mesh vertices with velocity field
    mesh: mesh
    T: vector function space
    vel: velocity field

    x -> x + epsilon * v(x)

    """
    x0_vert = mesh.coordinates()  # get mesh coordinates
    if x is None:
        v = fn.project(vel, T)
        x0_vert[:,0] = x0_vert[:,0] + epsilon * v.sub(0).compute_vertex_values(mesh)
        x0_vert[:,1] = x0_vert[:,1] + epsilon * v.sub(1).compute_vertex_values(mesh)
    else:
        x0_vert[:, 0] = x[:,0]
        x0_vert[:, 1] = x[:,1]
        # x0_vert = x.copy()


def calc_new_density(img, x0, x):
    """
    Calculate a new image.
    :param: x: new positions (N**2, 2)
    """
    # img1 = scipy.interpolate.griddata(x0, img.flatten(),
    #                                   x, method='nearest',
    #                                   fill_value='Nan')  # method could be linear...
    img2 = scipy.interpolate.griddata(x0, img.flatten(),
                                      x, method='linear',
                                      fill_value='Nan')  # method could be linear...
    # v = np.isnan(img2)
    # img2[v] = img1[v]
    return img2

def calc_new_vector(v, x0, x):

    v1 = v[:, 0]
    v2 = v[:, 1]

    vsh = np.shape(v)
    xsh = np.shape(x)
    v_new = np.zeros_like(x)

    v1_new = calc_new_density(v1, x0, x)
    v2_new = calc_new_density(v2, x0, x)

    v_new[:, 0] = v1_new
    v_new[:, 1] = v2_new

    return v_new


def convert_function_to_vector(f, V):

    f1 = fn.project(f, V)
    f1 = f1.vector()[:]
    sh = np.shape(f1)
    vect = np.zeros(sh)
    print(np.shape(vect))
    vect[:] = f1
    return vect

def convert_vector_to_function(vect, V):
    """
    Convert numpy vector to fenics function
    :param vect:
    :param V:
    :return:
    """
    img_fn = fn.Function(V)
    img_fn.vector()[:] = vect[:]
    return img_fn

def convert_image(img, V, mesh):
    """
    Outline
        Get dof coordinates
        Make pixel coordinates (between [0,1]x[0,1])
        Interpolate image at dof coordinates.

        ONLY FOR INITIAL IMAGES

    :param img:
    :param V:
    :return:
    """
    img = img.flatten()
    N = int(len(img)**0.5)
    # N = get_image_shape(img)
    x0 = mesh.coordinates()
    dim = np.ndim(x0)
    img_fn = fn.Function(V)
    dof_x = V.tabulate_dof_coordinates().reshape((-1, dim))  # dof positions
    lim = make_limits(x0, dim)
    img_x = make_initial_positions(N, dim, lim)
    img2 = calc_new_density(img, img_x, dof_x)
    img_fn.vector()[:] = img2[:]

    return img_fn


def make_image_positions(img, mesh):
    """
    Makes the image positions

    :param img:
    :param V:
    :return:
    """
    N = get_image_shape(img)
    x0 = mesh.coordinates()
    dim = np.ndim(x0)
    lim = make_limits(x0, dim)
    img_x = make_initial_positions(N, dim, lim)

    return img_x


def make_limits(x0, dim):
    lim = np.zeros((2, dim), dtype='float')
    lim[0, 0] = np.min(x0[:, 0])
    lim[1, 0] = np.max(x0[:, 0])
    lim[0, 1] = np.min(x0[:, 1])
    lim[1, 1] = np.max(x0[:, 1])

    return lim


def make_initial_positions(N, dim, lim):
    x1 = np.zeros((N, dim), dtype='float')
    for a in range(dim):
        x1[:, a] = np.linspace(lim[0, 0], lim[1, 0], N)
    if dim == 2:
        x2, y2 = np.meshgrid(x1[:, 0], x1[:, 1])
        x = np.zeros((N ** dim, 2), dtype='float')
        x[:, 0] = x2.flatten()
        x[:, 1] = y2.flatten()

    elif dim == 3:
        x2, y2, z2 = np.meshgrid(x1[:, 0], x1[:, 1], x1[:, 2])
        x = np.zeros((N ** dim, 3), dtype='float')
        x[:, 0] = x2.flatten()
        x[:, 1] = y2.flatten()
        x[:, 2] = z2.flatten()

    return x


def get_image_shape(img):
    sh = np.shape(img)
    if sh[0] == sh[1]:

        return sh[0]
    else:
        print('The image should be square')

        return sh[0]


######################################################################
def solve_2d_diffeo_problem(img, V):
    """
    Solve div(grad(f)) = I

    :param img:
    :param V:
    :return:
    """


    # Square the image and make sum(I^2) = 0
    # img0_squared1 = img.vector().array() #** 2
    # img0_squared1 = img0_squared1 - np.mean(img0_squared1)
    # img0_squared = fn.Function(V)
    # img0_squared.vector()[:] = img0_squared1[:]

    """ Make sum(I) = 0 """
    img0 = img.vector().array()
    img0 = img0 - np.mean(img0)
    img0_fn = fn.Function(V)
    img0_fn.vector()[:] = img0

    u = solve_2d_poisson(img0_fn, V)

    return u


# def grad(u, img, V, T):
#     """
#     u: fenics function
#     img: fenics function
#     V: function space
#     T: vector function space
#     Calculate grad(u)*img
#     """
#     vel = fn.Function(T)
#     img2 = np.zeros((len(img.vector().array(),)))
#     img[::2] = img.vector().array()
#     img[1::2] = img.vector().array()
#     v1 = fn.project(fn.grad(u), T)
#     vel.vector()[:] = v1 / img
#     return vel


def solve_2d_poisson(img, V):
    """
    Solve equation 2 from algorithm 1.

    nabla(f) = img_k

    """

    f = fn.TrialFunction(V)
    v = fn.TestFunction(V)

    a = fn.inner(fn.grad(v), fn.grad(f)) * fn.dx
    # L = img ** 2 * v * fn.dx
    L = img * v * fn.dx

    u = fn.Function(V)
    fn.solve(a == L, u)

    return u


def get_dof_positions(V, dim):
    """
    Get degrees of freedom positions from function space.
    :param V: function space
    :param dim: dimension (2 or 3)
    :return: dof positions in the form: (__, dim)
    """
    dof_x = V.tabulate_dof_coordinates().reshape((-1, dim))

    return dof_x


def interpolate_image_new_positions(img, x0, x):
    """
    Interpolate image at new positions.

    :param img: image
    :param x0: original positions
    :param x: new positions
    :return: image (vector with shape: (N^2, 1))
    """

    N = int(np.shape(x)[0]**0.5)  # linear dimension
    # img1 = scipy.interpolate.griddata(x0, img.flatten(),
    #                                   x, method='nearest',
    #                                   fill_value='Nan')  # method could be linear...
    img2 = scipy.interpolate.griddata(x0, img.flatten(),
                                      x, method='linear',
                                      fill_value='Nan')  # method could be linear...
    # v = np.isnan(img2)
    # img2[v] = img1[v]
    # img2 = img2.reshape((N, N))

    return img2


def interp_and_convert_image(img, x0, x, V, mesh):
    """
    Interpolates and convnerts image to FEniCS function.

    :param img: image
    :param x0: original positions
    :param x: new positions
    :param V: function space
    :param mesh: mesh
    :return: FEniCS function to using in PDE
    """
    img2 = interpolate_image_new_positions(img, x0, x)
    img2_fn = fn.Function(V)
    img2_fn.vector()[:] = img2[:]

    return img2_fn


def grad(f, img, dim):
    """
    Calculates a gradient with a non-euclidean metric tensor (g).

    see: 'Diffeomorphic density matching by optimal information transport' by Martin Bauer et. al.
    Appendix: A.1

    g = img^(2/dim) * identity

    """

    grad_f = img**(-dim/2)*fn.grad(f)

    return grad_f


def change_pos(x, lim):
    """
    Make sure coordinates stay within the T^2 (2D torus) s.t. phi: M -> M

    :param x: positions
    :param epsilon:
    :param vel: velocity
    :param lim:
    :return: New positions that fall withing T^2 (2D torus)
    """

    # make sure the coordinates map back onto themselves
    v1 = x[:, 0] < lim[0, 0]
    v2 = x[:, 0] > lim[1, 0]
    v3 = x[:, 1] < lim[0, 1]
    v4 = x[:, 1] > lim[1, 1]

    d0 = lim[1, 0] - lim[0, 0]
    d1 = lim[1, 1] - lim[0, 1]

    # x[v1, 0] = x[v1, 0] % d0 + d0
    # x[v2, 0] = x[v2, 0] % d0 - d0
    # x[v3, 1] = x[v3, 1] % d1 + d1
    # x[v4, 1] = x[v4, 1] % d1 - d1

    # Should be fixed if the point is moved very far away (then it will be something like x%d0 + d0)
    x[v1, 0] = x[v1, 0] + d0
    x[v2, 0] = x[v2, 0] - d0
    x[v3, 1] = x[v3, 1] + d1
    x[v4, 1] = x[v4, 1] - d1

    # print('min and max positions are: ', np.min(x), np.max(x))

    return x


def make_mu_dot_over_mu_imgs(img0, img1, t, lim):
    """

    :param img0: first image
    :param img1: second image
    :param t: times
    :param lim: limits
    :return: list of numpy arrays
    """

    vol_m = np.abs(lim[0,0] - lim[1,0]) * np.abs(lim[0,1] - lim[1,1])  # manifold volume

    img0 = img0 - integrate(img0, lim) / vol_m + vol_m
    img1 = img1 - integrate(img1, lim) / vol_m + vol_m


    N = img0.shape[0]
    lngth = np.abs(lim[0,0] - lim[1,0])/N  # distance between each pixel

    f0, f1 = np.sqrt(img0), np.sqrt(img1)


    # f0 = f0 * vol_m / np.sqrt(integrate(f0**2.0, lim))
    # f1 = f1 * vol_m / np.sqrt(integrate(f1**2.0, lim))

    theta = np.arccos(1.0/vol_m * np.sum(f0 * f1) * lngth**2)

    mu_dot_over_mu = []
    for a in t:
        numerator = 2.0 * theta * (np.cos(theta - a * theta) * f0 - np.cos(a * theta) * f1)
        denominator = f1 * np.sin(a * theta) + np.sin(theta - a * theta) * f0
        mu_dot_over_mu.append(numerator / denominator)

    return mu_dot_over_mu


def normalize_image(img, lim):
    """
    Set integral of image equal to manifold volume.
    :param img: image
    :param lim: limits,
        1st index: start or end,
        2nd index: x or y
    :return:
    """

    vol_m = np.abs(lim[0,0] - lim[1,0]) * np.abs(lim[0,1] - lim[1,1])  # manifold volume
    # img2 = img - integrate(img, lim) + vol_m
    # img2 = img - np.mean(img) + vol_m
    img2 = img - integrate(img, lim) / vol_m + vol_m

    return img2


def integrate(f, lim):

    N = f.shape[0]
    lngth = np.abs(lim[0,0] - lim[1,0])/N  # distance between each pixel
    f_integral = lngth**2 * np.sum(f)

    return f_integral


def save_mesh_fig(mesh, dpi=100, sz=(12,10), filename='mesh'):
    import matplotlib.tri as tri

    # Create the triangulation
    mesh_coordinates = mesh.coordinates().reshape((-1, 2))
    triangles = np.asarray([cell.entities(0) for cell in fn.cells(mesh)])
    triangulation = tri.Triangulation(mesh_coordinates[:, 0],
                                      mesh_coordinates[:, 1],
                                      triangles)

    # Plot the mesh
    fig = plt.figure(figsize=sz)
    plt.triplot(triangulation)
    plt.savefig(filename + r'.png', dpi=dpi)
    plt.title('Mesh')
    plt.pause(10)
