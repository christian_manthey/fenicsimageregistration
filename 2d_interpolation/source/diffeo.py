"""
Diffeomorphism class

"""

import fenics as fn
import numpy as np
from source.utils import (interpolate_image_new_positions, change_pos, fn_to_np, fn_to_np_at_mesh_vert,
calc_new_density, calc_new_vector, flatten_np_for_fn)

class Diffeo(object):
    def __init__(self, vel_x0, lim, V, T, mesh, dim):
        """

        :param vel_x0: dof positions
        :param lim: limits
        :param V: function space
        :param mesh: mesh
        :param dim: dimension (2 or 3)
        """

        self.V = V
        self.T = T
        self.lim = lim
        self.x = vel_x0.copy()
        self.mesh_x0 = mesh.coordinates().copy()  # shape: (N_vert, dim)
        self.dof_x0 = V.tabulate_dof_coordinates().reshape((-1, dim))  # shape: (N_dof, dim)

        self.phi_dof_x = self.dof_x0.copy()
        self.phi_dof_xm = self.dof_x0.copy() # phi at dof (mapped back onto M)
        self.phi_mesh_x = self.mesh_x0.copy()

        self.inv_phi_dof_x = self.dof_x0.copy()
        self.inv_phi_dof_xm = self.dof_x0.copy()  # inverse phi at dof (mapped back onto M)
        self.inv_phi_mesh_x = self.mesh_x0.copy()

        self.psi_dof_x = self.dof_x0.copy()
        self.psi_mesh_x = self.mesh_x0.copy()

        self.inv_psi_dof_x = self.dof_x0.copy()
        self.inv_psi_mesh_x = self.mesh_x0.copy()


        # Displacements
        """ Update displacements each time with epsilon*velocity for mesh transformation. """
        self.d_phi_mesh_x = np.zeros_like(self.mesh_x0)
        self.d_inv_phi_mesh_x = np.zeros_like(self.mesh_x0)
        self.d_psi_mesh_x = np.zeros_like(self.mesh_x0)
        self.d_inv_psi_mesh_x = np.zeros_like(self.mesh_x0)

        self.d_phi_dof_x = np.zeros_like(self.dof_x0)
        self.d_inv_phi_dof_x = np.zeros_like(self.dof_x0)
        self.d_psi_dof_x = np.zeros_like(self.dof_x0)
        self.d_inv_psi_dof_x = np.zeros_like(self.dof_x0)


    def update_phi(self):
        """
        Update phi(x) & inverse phi at dof & mesh vertices
        :return:
        """
        self.update_phi_dof()
        self.update_phi_mesh()

    def update_psi(self, vel, epsilon, T, mesh):
        self.update_psi_dof(vel, epsilon, T)
        self.update_psi_mesh(vel, epsilon, T, mesh)

    def update_psi_dof(self, vel, epsilon, T):
        """
        Update psi & inverse psi at dofs.
        :param vel:
        :param epsilon:
        :param T:
        :return:
        """
        v = fn_to_np(vel, T, 2)
        self.psi_dof_x = self.dof_x0 + epsilon * v  # psi evaluated at dof
        self.inv_psi_dof_x = self.dof_x0 - epsilon * v  # inverse psi evaluated at dof


        # Update displacements
        self.d_psi_dof_x = epsilon * v
        self.d_inv_psi_dof_x = -epsilon * v


        # Move point back onto T^2 (original manifold)
        self.psi_dof_x = change_pos(self.psi_dof_x, self.lim)
        self.inv_psi_dof_x = change_pos(self.inv_psi_dof_x, self.lim)

    def update_psi_mesh(self, vel, epsilon, T, mesh):
        """
        Update psi & inverse psi at mesh vertices.
        :param vel:
        :param T:
        :param mesh:
        :param epsilon:
        :return:
        """
        v = fn_to_np_at_mesh_vert(vel, T, mesh)
        self.psi_mesh_x = self.mesh_x0 + epsilon * v
        self.inv_psi_mesh_x = self.mesh_x0 - epsilon * v

        """ Update the displacements at each mesh vertex (then add displacements at the end for final mesh deformation)"""
        self.d_psi_mesh_x = epsilon * v
        self.d_inv_psi_mesh_x = - epsilon * v

    def update_phi_dof(self):
        """
        Update phi & inverse phi at dofs.
        :return:
        """
        # self.phi_dof_x = calc_new_vector(self.psi_dof_x, self.dof_x0, self.phi_dof_x)
        # self.inv_phi_dof_x = calc_new_vector(self.inv_phi_dof_x, self.dof_x0, self.inv_psi_dof_x)

        self.d_phi_dof_x = self.d_phi_dof_x + \
                           calc_new_vector(self.d_psi_dof_x, self.dof_x0, change_pos(self.phi_dof_x, self.lim))
        self.d_inv_phi_dof_x = self.d_inv_psi_dof_x + \
                               calc_new_vector(self.d_inv_phi_dof_x, self.dof_x0, change_pos(self.inv_psi_dof_x, self.lim))

        self.phi_dof_x = self.dof_x0 + self.d_phi_dof_x
        self.inv_phi_dof_x = self.dof_x0 + self.d_inv_phi_dof_x

        # Move point back onto T^2 (original manifold)
        self.phi_dof_xm = change_pos(self.phi_dof_x, self.lim)
        self.inv_phi_dof_xm = change_pos(self.inv_phi_dof_x, self.lim)

    def update_phi_mesh(self):
        """
        Update phi & inverse phi at the mesh vertices.
        :param vel:
        :param epsilon:
        :param T:
        :return:
        """
        # self.phi_mesh_x = calc_new_vector(self.psi_mesh_x, self.mesh_x0, self.phi_mesh_x)
        # self.inv_phi_mesh_x = calc_new_vector(self.inv_phi_mesh_x, self.mesh_x0, self.inv_psi_mesh_x)

        """ Update the displacements at each mesh vertex (then add displacements at the end for final mesh deformation)"""
        self.d_phi_mesh_x = \
            self.d_phi_mesh_x + \
            calc_new_vector(self.d_psi_mesh_x, self.mesh_x0, change_pos(self.phi_mesh_x, self.lim))

        self.d_inv_phi_mesh_x = \
            self.d_inv_psi_mesh_x + \
            calc_new_vector(self.d_inv_phi_mesh_x, self.mesh_x0, change_pos(self.inv_psi_mesh_x, self.lim))

    def calc_phi_dof_from_displacements(self):
        self.phi_dof_x = self.dof_x0 + self.d_phi_dof_x
        self.inv_phi_dof_x = self.dof_x0 + self.d_inv_phi_dof_x

    def calc_phi_mesh_from_displacements(self):
        """
        Calculates phi & inverse phi from displacements.
        :return:
        """
        self.phi_mesh_x = self.mesh_x0 + self.d_phi_mesh_x
        self.inv_phi_mesh_x = self.mesh_x0 + self.d_inv_phi_mesh_x


    def calc_det_jac(self):

        self.phi_fn = fn.Function(self.T)
        self.inv_phi_fn = fn.Function(self.T)
        self.d_phi_fn = fn.Function(self.T)
        self.d_inv_phi_fn = fn.Function(self.T)

        # self.phi_fn.vector()[:] = flatten_np_for_fn(self.phi_dof_x)
        # self.inv_phi_fn.vector()[:] = flatten_np_for_fn(self.inv_phi_dof_x)


        identity = fn.Identity(len(self.d_inv_phi_fn))

        self.d_phi_fn.vector()[:] = flatten_np_for_fn(self.d_phi_dof_x)
        self.d_inv_phi_fn.vector()[:] = flatten_np_for_fn(self.d_inv_phi_dof_x)

        self.det_jac_phi = fn.det(fn.grad(self.d_phi_fn) + identity)
        self.det_jac_inv_phi = fn.det(fn.grad(self.d_inv_phi_fn) + identity)


    def pull_back(self, mu0, x0):
        """
        Convert mu0 -> mu1 through pushforward transformation.

        :param mu0: initial density, numpy array with shape: ( , )
        :param x0: initial positions of mu0
        :return:
        """
        # Compose initial density (numpy array)
        # mu = calc_new_density(mu0, x0, self.phi_dof_xm)

        """ Check this """
        mu = calc_new_density(mu0, x0, self.phi_dof_xm)
        # Convert to fenics function
        mu_fn = fn.Function(self.V)
        mu_fn.vector()[:] = mu

        self.mu1 = self.det_jac_phi * mu_fn

        # self.mu1 = (1.0 + self.d_inv) * mu_fn
        return self.mu1

    def make_conformal_metric(self, img0, img0_x0):
        # h = I g
        img_dof = calc_new_density(img0, img0_x0, self.dof_x0)
        img_mesh = calc_new_density(img0, img0_x0, self.mesh_x0)
        img_x_dof = calc_new_vector(img0_x0, img0_x0, self.dof_x0)
        img_x_mesh = calc_new_vector(img0_x0, img0_x0, self.mesh_x0)
        for a in range(2):
            self.d_phi_dof_x[:, a] = img_dof.flatten() - img_x_dof[:,a]
            self.d_inv_phi_dof_x[:, a] = -img_dof.flatten() - img_x_dof[:,a]
            self.d_phi_mesh_x[:, a] = img_mesh.flatten() - img_x_mesh[:,a]
            self.d_inv_phi_mesh_x[:, a] = -img_mesh.flatten() - img_x_mesh[:,a]
